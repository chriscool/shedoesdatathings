---
title: Twelve in Nineteen.
date: 2019-01-19 00:00:00 -0500
tags: []
image: ''

---
In my career, I've been lucky enough to be surrounded by incredible minds who have been nothing but generous with their time and knowledge. I feel blessed (not in the Instagram) way to have a myriad of professional mentors who have shaped my understanding and mental models to technical problems. Today, I'm lucky to be surrounded by a powerful team of individual contributors who know. their. stuff. And I get to learn from them. 

I don't know a lot, but I do know somethings and have strong opinions on others. I often hit problems, though, so I have experiences that are useful, if only because they are not straightforward. This year, 2019, I'm committed to sharing some of how I broke through those walls. Specifically, I'm committing to **sharing 12 technical blog posts here in 2019.** 

I am increasingly struck by the idea that technical blogs are just a way of building your own technical documentation. You write about the problem you had and how you solved it, in part to helps others who might stumble upon but also to help yourself if you find yourself in that predicament again.

## This is my contribution.

I'll mention in passing that I _already_ write a ton about goal-setting, productivity, workflow, apps, podcasts, and all the other things you read about on personal blogs on my lifestyle blog [She Does Better](shedoesbetter.com), which started as something to do as I procrastinated writing my thesis. Four years later, that thing is still alive and kicking. That will be where I continue to write _most of the time_. This is just for the technical things. 

I gave my second-ever technical talk this week at [TechSAV's Lunch Function](https://techsav.co/2019/01/18/lunch-function-101dataops-devops-for-data-engineering/). The talk was titled "DataOps: DevOps for Data Engineering."_Thank you to Sarah Pounders and the TechSAV community for the opportunity!_

<center><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/4u2l9tzMlis" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>

I don't know what my goal with these 12 blog posts will be, but for now it's how I can do my part in contributing to the ecosystem. 

_A special thanks to_ [_Priyanka_](http://twitter.com/pritianka/) _for the positive peer pressure to push myself outside of my comfort zone._ 
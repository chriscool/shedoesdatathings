---
date: 2019-05-04T04:00:00+00:00
tags:
- conference notes
- WIA2019
photo: "/uploads/women_in_analytics_conf_hall.jpeg"
title: Words of Wisdom from the Women in Analytics Conference

---
When I first got wind of Facebook's [Women in Analytics Conference](https://womeninanalytics2019.splashthat.com/) a couple of months ago, I got really excited. I had already made plans to attend [Data Council](http://datacouncil.ai) (formerly DataEngConf) in SF that week, so extending the stay made this a much more affordable marginal expense. This was the first event I'd ever heard specifically _for_ women in data. While I'm really proud of my success in building an extensive professional network through the internet, it is certainly not a **diverse** network. I've tried to do more- like going out of my way to make sure I'm following more diverse voices on Twitter and in my RSS feed- but my network is a representation of the realities of data as an industry.

![](/uploads/women_in_analytics_conf_sign.jpeg)

I'm not sure how I stumbled on the application for the conference (I think I applied back in December?), but I'm so glad I did.

Conference organizers [Nell Thomas](https://www.linkedin.com/in/nellwyn-thomas/) and [Jenni Romanek](https://www.linkedin.com/in/jenni-romanek-a37020b/) opened up the event by reminding the audience of roughly 600 with their motivation:

* Representation matters
* Learning matters
* Networking matters

![](/uploads/women_in_analytics_conf_fireside.jpeg)

## Panel Discussion

* "When I started my career, I felt like I was a failure already, I was doing something wrong." -Yuko Yamakazi
* "If you're going into a meeting, make sure you have at least 1 thing to say. If you don't have anything to say, there's no point to being in that meeting anyway." - Amy Sample
* "Don't get distracted by all the new research, the new things coming out- stay focused on your goals: building data products and shipping." - Omoju Miller
* Work Life Balance
  * Doesn't happen in any given day, but happens over the ebb and flow of weeks (very similar to Laura Vanderkam's framing!)
  * Work you need to be doing is important enough
  * The Life can still enrich your work
  * Completely disconnecting when disconnecting
* Success is Analytics
  * Curious & inquisitive, Technical only sometimes a requirement
  * higher up in leadership roles, the less time you'll be doing math
  * emphasize storytelling in delivering
  * impact analytics- analytics along is not enough
* Community
  * connecting
  * elevating the work of other women
* Next hardest problems
  * inclusion- not just women but serving women of color
  * more female founded  companies

![](/uploads/women_in_analytics_conf_taccc.jpeg)

## Lightning Talks

Angela Song

* Careers are not tracks, they're jungle gyms
* Winners want the ball
* Winners bounce back from defeat
* "We cannot connect the docs looking forward, we can only connect them looking back."
* Winning is staying in the game

Jennifer Prendki

* Cynics at work
* You have to clean up your data before you use it
* Alectio: Sustainable Machine Learning
* Lead the way until it hurts

Hilary Parker:

* import, tidy, (explore, model, visualize), communicate
* design way of thinking
* building a whole system
* Design science and art form an "and" not an "or" relationship to created the incredibly human cognitive ability - Nigel Cross
* deductive thinking - solving a problem
* design thinking - iterating through an idea

![](/uploads/women_in_analytics_conf_schedule.jpeg)

## Fireside Chat

* realistic expectations of what mentorship is
* only do what needs to be done
* do it a little bit worse and a little bit faster

![](/uploads/women_in_analytics_conf_breakout_schedule.jpeg)

## Breakout Sessions

**Data Governance:**

* The Panopticon - system of control designed
* Bit by Bit
* evidon.com/solutions/trackermap
* Data governance is ethics at schale
* Principled decision making
* Minimize data being collected
* Tech is advancing faster than laws
* Principles to shape data governance
  * Respect
  * Thoughtful consideration
  * Trasnparency
* How comfortable would you be documenting your data practices on the front page of the New York Times?

**Ethics of Engineering:**

* Culture has celebrated "innovation" coming out of white men
* Ford Pinto

![](/uploads/women_in_analytics_conf_claire.jpeg)

I learned so much at the WiA Conference at Facebook earlier this year. While I don't condone Facebook's data practices in many ways, it was generous of the organization to lend it's leaders and financial support to the event. I hope it happens again!